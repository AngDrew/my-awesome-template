import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mc_item_json_gen/src/logic/constants/routes_c.dart';
import 'package:mc_item_json_gen/src/logic/services/config_service.dart';
import 'package:mc_item_json_gen/src/logic/services/language_service.dart';
import 'package:mc_item_json_gen/src/logic/services/theme_service.dart';
import 'package:mc_item_json_gen/src/logic/utils/routes.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'l10n/gen_l10n/app_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildWidget>[
        ChangeNotifierProvider<ThemeService>(
          create: (BuildContext context) => ThemeService(),
        ),
        //
        ChangeNotifierProvider<LanguageService>(
          create: (BuildContext context) => LanguageService(),
        ),
        //
        ChangeNotifierProxyProvider2<LanguageService, ThemeService,
            ConfigService>(
          create: (BuildContext context) => ConfigService(
            language: context.read<LanguageService>(),
            theme: context.read<ThemeService>(),
          ),
          update: (
            BuildContext context,
            LanguageService value,
            ThemeService value2,
            ConfigService? previous,
          ) =>
              ConfigService(
            language: value,
            theme: value2,
          ),
        ),
      ],
      child: Consumer<ConfigService>(
        builder: (
          BuildContext context,
          ConfigService value,
          Widget? child,
        ) =>
            MaterialApp(
          title: 'Project Name',
          onGenerateRoute: onGenerateRoute,
          initialRoute: rootRoute,
          theme: value.theme.lightTheme,
          localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
            AppLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          // supportedLocales: AppLocalizations.supportedLocales,
          supportedLocales: const <Locale>[
            Locale('en'),
          ],
          locale: value.language.locale,
        ),
      ),
    );
  }
}
