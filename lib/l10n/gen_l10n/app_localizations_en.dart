


import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get generateL10N => 'flutter gen-l10n';

  @override
  String get applicationTitle => 'AppName';
}
