import 'package:intl/intl.dart';

class MoneyFormatter {
  /// Rp x.xxx
  static String idrMoneyFormat(num val) {
    return NumberFormat.currency(
      locale: 'idID',
      decimalDigits: 0,
      symbol: 'Rp ',
    ).format(val);
  }

  /// x,xxx
  static String simpleMoneyFormat(num val) {
    final NumberFormat formatter = NumberFormat('#,###');

    return formatter.format(val);
  }
}
