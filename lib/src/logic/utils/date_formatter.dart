import 'package:intl/intl.dart';

class DateTimeFormatter {
  /// format: `EEEEEE, d/M/y HH:mm:ss`
  static String completeDateFormatter(DateTime date) {
    return DateFormat('EEEEEE, d-M-y HH:mm:ss', 'idID').format(date);
  }

  /// format: `HH:mm:ss`
  static String timeFormatter(DateTime date) {
    return DateFormat('HH:mm:ss').format(date);
  }

  /// format: `HH:mm`
  static String shortTimeFormatter(DateTime date) {
    return DateFormat('HH.mm').format(date);
  }

  /// format: `d-M-y`
  static String dateFormatter(DateTime date) {
    return DateFormat('d-M-y').format(date);
  }

  /// format: `d MMM yyyy`
  static String dateFormatterNamed(DateTime date) {
    return DateFormat('d MMM yyyy').format(date);
  }

  /// format: `d-M-y HH:mm:ss`
  static String dateTimeFormatter(DateTime date) {
    return DateFormat('d-M-y HH:mm:ss').format(date);
  }

  /// format: `EEE, d MMM y`
  static String dateFormatterWithDay(DateTime date) {
    return DateFormat('EEE, d MMM y', 'idID').format(date);
  }

  // reversed

  /// format: `y-MM-dd`
  static String dateFormatterReverse(DateTime date) {
    return DateFormat('y-MM-dd').format(date);
  }

  /// format: `y-M-d HH:mm:ss`
  static String dateTimeFormatterReversedDate(DateTime date) {
    return DateFormat('y-M-d HH:mm:ss').format(date);
  }
}
