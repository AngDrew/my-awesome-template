import 'package:app/src/views/resources/r.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

mixin DialogMixin {
  void showMySnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: R.styles.normalFont.copyWith(color: whiteColor),
        ),
      ),
    );
  }

  void showMyBottomSheet(BuildContext context, Widget child) {
    showModalBottomSheet<void>(
      context: context,
      builder: (_) => child,
      backgroundColor: Colors.transparent,
    );
  }
}
