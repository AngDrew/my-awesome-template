import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';

mixin AfterFirstBuildMixin<T extends StatefulWidget> on State<T> {
  void afterFirstBuild(BuildContext context);

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback(
      (Duration timeStamp) {
        // print('Took ${timeStamp.inMilliseconds}ms to build');
        afterFirstBuild(context);
      },
    );
  }
}
