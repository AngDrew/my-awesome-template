import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

mixin RequestHandlerMixin on ChangeNotifier {
  Future<bool> onResponse(
    Response<dynamic>? resp,
    Future<void> Function() onSuccess, {
    int successStatusCode = 200,
  }) async {
    if ((resp?.statusCode ?? -1) == successStatusCode) {
      await onSuccess();
      notifyListeners();

      return true;
    } else {
      return false;
    }
  }
}
