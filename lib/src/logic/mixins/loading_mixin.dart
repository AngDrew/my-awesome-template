import 'package:flutter/foundation.dart';

/// remember to always check the loading prefix
mixin LoadingMixin on ChangeNotifier {
  String classPrefix = '[Loading] ';

  void specifyLoadingPrefix(String className) {
    classPrefix = '[Loading $className] ';
  }

  /// used to implements loading

  bool _isLoading = false;

  /// set loading to true when it's not loading
  void mayLoading({String? msg}) {
    if (!_isLoading) {
      _isLoading = true;
    }
    print('${msg != null ? '$msg -> ' : ''}$classPrefix is now loading');
    notifyListeners();
  }

  /// set loading to false when it's loading
  void finishLoading({String? msg}) {
    if (_isLoading) {
      _isLoading = false;
    }
    print('${msg != null ? '$msg -> ' : ''}$classPrefix has been loaded');
    notifyListeners();
  }

  bool get isLoading => _isLoading;
  bool get isNotLoading => !_isLoading;
}
