mixin LogMessageMixin {
  void logMessage(String message) => print('[$classPrefix] $message');
  String? classPrefix;
}
