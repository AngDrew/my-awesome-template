import 'package:app/src/logic/services/api.dart';
import 'package:app/src/logic/services/global_key_service.dart';
import 'package:app/src/logic/services/network_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton(() => NetworkService());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => GlobalKeyService());

  // TODO<Urgent>: Global Key
}
