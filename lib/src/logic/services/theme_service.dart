import 'package:app/src/logic/view_models/base_vm.dart';
import 'package:app/src/logic/enums/theme_state.dart';
import 'package:app/src/views/resources/colors.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeService extends BaseViewModel {
  ThemeService() {
    init();

    lightColorScheme = const ColorScheme(
      primary: primaryLight,
      primaryVariant: primaryLightPressed,
      secondary: secondaryLightPressed,
      secondaryVariant: secondaryLightPressed,
      surface: surfaceLight,
      background: backgroundLight,
      error: errorLight,
      onPrimary: onPrimaryLight,
      onSecondary: onSecondaryLight,
      onSurface: onSurfaceLight,
      onBackground: onBackgroundLight,
      onError: onErrorLight,
      brightness: Brightness.light,
    );

    darkColorScheme = const ColorScheme(
      primary: primaryDark,
      primaryVariant: primaryDarkPressed,
      secondary: secondaryDarkPressed,
      secondaryVariant: secondaryDarkPressed,
      surface: surfaceDark,
      background: backgroundDark,
      error: errorDark,
      onPrimary: onPrimaryDark,
      onSecondary: onSecondaryDark,
      onSurface: onSurfaceDark,
      onBackground: onBackgroundDark,
      onError: onErrorDark,
      brightness: Brightness.dark,
    );

    lightTheme = ThemeData(
      // primarySwatch: CustomSwatch.customLightSwatch,
      colorScheme: lightColorScheme,
      backgroundColor: backgroundLight,
      scaffoldBackgroundColor: backgroundLight,
      primaryColor: primaryLight,
      accentColor: secondaryLight,
      appBarTheme: const AppBarTheme(
        backgroundColor: surfaceLight,
        foregroundColor: onSurfaceLight,
        elevation: 0,
      ),
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
    );

    darkTheme = ThemeData(
      // primarySwatch: CustomSwatch.customDarkSwatch,
      colorScheme: darkColorScheme,
      backgroundColor: backgroundDark,
      scaffoldBackgroundColor: backgroundDark,
      primaryColor: primaryDark,
      accentColor: secondaryDark,
      appBarTheme: const AppBarTheme(
        backgroundColor: backgroundDark,
        foregroundColor: onBackgroundDark,
        elevation: 0,
      ),
      pageTransitionsTheme: const PageTransitionsTheme(
        builders: <TargetPlatform, PageTransitionsBuilder>{
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
        },
      ),
    );
  }

  Future<void> init() async {
    _sharedPref = await SharedPreferences.getInstance();

    if (_sharedPref.getBool('isDarkTheme') == null) {
      await _sharedPref.setBool('isDarkTheme', false);
    }

    if (_sharedPref.getBool('isDarkTheme') != null &&
        _sharedPref.getBool('isDarkTheme')!) {
      _currentTheme = ThemeState.dark;
    } else {
      _currentTheme = ThemeState.light;
    }

    refreshState();
  }

  late final SharedPreferences _sharedPref;

  ThemeState _currentTheme = ThemeState.light;
  ThemeState get currentTheme => _currentTheme;

  late final ThemeData lightTheme;
  late final ThemeData darkTheme;

  late final ColorScheme lightColorScheme;
  late final ColorScheme darkColorScheme;

  void toggleThemeState() {
    if (_currentTheme == ThemeState.light) {
      _currentTheme = ThemeState.dark;
      _sharedPref.setBool('isDarkTheme', true);
    } else if (_currentTheme == ThemeState.dark) {
      _currentTheme = ThemeState.light;
      _sharedPref.setBool('isDarkTheme', false);
    }
    refreshState();
  }

  bool? get isDarkTheme => _sharedPref.getBool('isDarkTheme');
}
