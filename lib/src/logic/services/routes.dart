import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import '../../views/layouts/mobile/screens/landing/landing_v.dart';
import '../../views/layouts/mobile/screens/landing/landing_vm.dart';
import '../../views/layouts/mobile/screens/onboarding/onboarding_v.dart';
import '../../views/layouts/mobile/screens/onboarding/onboarding_vm.dart';
import '../../views/layouts/mobile/screens/splash/splash_v.dart';
import '../../views/layouts/mobile/screens/splash/splash_vm.dart';
import 'api.dart';

const String rootRoute = '/';
const String loginRoute = 'login';
const String registerRoute = 'register';
const String forgotPasswordRoute = 'fpassword';
const String otpRoute = 'otp';
const String landingRoute = 'landing';
const String onboardingRoute = 'onboarding';

/// use the constants for route naming to make development easier
/// and avoid human error
Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case rootRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) =>
            ChangeNotifierProvider<SplashViewModel>(
          create: (BuildContext context) => SplashViewModel(),
          builder: (BuildContext context, Widget? child) =>
              const SplashScreen(),
        ),
      );
    case landingRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) => MultiProvider(
          providers: <SingleChildWidget>[
            ChangeNotifierProvider<LandingViewModel>(
              create: (BuildContext context) => LandingViewModel(),
            ),
          ],
          builder: (_, __) => LandingScreen(),
        ),
      );
    case onboardingRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) =>
            ChangeNotifierProvider<OnboardingViewModel>(
          create: (_) => OnboardingViewModel(),
          builder: (_, __) => const OnboardingScreen(),
        ),
      );
    default:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (_) => const SplashScreen(),
      );
  }
}
