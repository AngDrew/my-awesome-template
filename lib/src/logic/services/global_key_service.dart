import 'package:app/src/logic/utils/custom_border_radius.dart';
import 'package:app/src/views/resources/r.dart';
import 'package:flutter/material.dart';

class GlobalKeyService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  BuildContext? get currentContext => navigatorKey.currentState?.context;

  Future<dynamic> showTheDialog(Widget dialog) async {
    if (navigatorKey.currentState != null) {
      final BuildContext currentContext = navigatorKey.currentState!.context;
      await showDialog<dynamic>(
        context: currentContext,
        builder: (BuildContext context) => dialog,
      );
    }
  }

  Future<dynamic> showTheSnackBar(String message) async {
    if (navigatorKey.currentState != null) {
      final BuildContext currentContext = navigatorKey.currentState!.context;

      final ColorScheme csc = Theme.of(currentContext).colorScheme;

      ScaffoldMessenger.of(currentContext).showSnackBar(
        SnackBar(
          backgroundColor: csc.error,
          content: Text(
            message,
            style: R.styles.normalFont.copyWith(color: csc.onError),
          ),
          margin: R.spaces.betterSpaceAround,
          shape: RoundedRectangleBorder(
            borderRadius: CustomBorderRadius.all(),
          ),
          behavior: SnackBarBehavior.floating,
        ),
      );
    }
  }
}
