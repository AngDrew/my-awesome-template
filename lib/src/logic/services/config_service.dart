import 'package:app/src/logic/services/language_service.dart';
import 'package:app/src/logic/services/theme_service.dart';
import 'package:app/src/logic/view_models/base_vm.dart';

class ConfigService extends BaseViewModel {
  ConfigService({required this.language, required this.theme});

  LanguageService language;
  ThemeService theme;
}
