import 'package:flutter/material.dart';
import 'package:mc_item_json_gen/src/view/resources/customswatch.dart';

ThemeData myLightTheme = ThemeData(
  primarySwatch: customswatch,
  colorScheme: const ColorScheme(
    primary: Color(0xFFF3D5C0),
    primaryVariant: Color(0xFFF3D5C0),
    //
    secondary: Color(0xFFD4B499),
    secondaryVariant: Color(0xFFD4B499),
    //
    surface: Color(0xFF889EAF),
    background: Color(0xFF506D84),
    error: Color(0xFF630A10),
    //
    onPrimary: Color(0xFF506D84),
    onSecondary: Color(0xFF889EAF),
    //
    onSurface: Color(0xFFF3D5C0),
    onBackground: Color(0xFFF3D5C0),
    onError: Color(0xFFFCF0C8),
    //
    brightness: Brightness.light,
  ),
);
// ThemeData myDarkTheme = ThemeData(
//   colorScheme: const ColorScheme(
//     primary: Color(0xFF000000),
//     primaryVariant: Color(0xFF000000),
//     //
//     secondary: Color(0xFF000000),
//     secondaryVariant: Color(0xFF000000),
//     //
//     surface: Color(0xFF000000),
//     background: Color(0xFF000000),
//     error: Color(0xFF000000),
//     //
//     onPrimary: Color(0xFF000000),
//     onSecondary: Color(0xFF000000),
//     //
//     onSurface: Color(0xFF000000),
//     onBackground: Color(0xFF000000),
//     onError: Color(0xFF000000),
//     //
//     brightness: Brightness.light,
//   ),
// );
