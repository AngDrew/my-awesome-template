import 'package:app/src/logic/view_models/base_vm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

class BaseView<T extends BaseViewModel> extends StatefulWidget {
  const BaseView({
    required this.builder,
    this.onReady,
    Key? key,
  }) : super(key: key);

  final Widget Function(BuildContext context, T vm, Widget? child) builder;
  final Future<void> Function(T vm)? onReady;

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseViewModel> extends State<BaseView<T>> {
  late final T vm = context.read<T>();

  @override
  void initState() {
    super.initState();
    final DateTime start = DateTime.now();
    late final DateTime end;

    SchedulerBinding.instance?.addPostFrameCallback(
      (Duration timeStamp) async {
        await widget.onReady?.call(vm);
        end = DateTime.now();

        print(
          '[$T] took ${end.difference(start).inMilliseconds}ms '
          'for build and init',
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: Consumer<T>(
        builder: widget.builder,
      ),
    );
  }
}
