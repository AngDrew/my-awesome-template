import 'package:flutter/material.dart';

import '../resources/r.dart';
import 'buttons/elevated_buttons.dart';
import 'buttons/outlined_button.dart';

class MyChip extends StatelessWidget {
  const MyChip({
    Key? key,
    required this.label,
    this.isActive = false,
    this.onPressed,
    this.color,
    this.icon,
  }) : super(key: key);

  final bool isActive;
  final String label;
  final VoidCallback? onPressed;
  final Color? color;
  final IconData? icon;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    final Widget child = Text(
      label,
      style: R.styles.normalFont.copyWith(
          color: isActive
              ? theme.colorScheme.onPrimary
              : theme.colorScheme.onSurface),
    );

    if (isActive) {
      return RoundedElevatedButton(
        padding: R.spaces.tinySpaceAround,
        buttonColor: color,
        onPressed: onPressed,
        child: icon == null
            ? child
            : Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Icon(
                    icon,
                    color: isActive
                        ? theme.colorScheme.onPrimary
                        : theme.colorScheme.onSurface,
                  ),
                  child,
                ],
              ),
      );
    }

    return RoundedOutlinedButton(
      padding: R.spaces.tinySpaceAround,
      buttonColor: color,
      onPressed: onPressed,
      child: icon == null
          ? child
          : Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  icon,
                  color: isActive
                      ? theme.colorScheme.onPrimary
                      : theme.colorScheme.onSurface,
                ),
                child,
              ],
            ),
    );
  }
}
