import 'package:app/src/views/resources/r.dart';
import 'package:flutter/material.dart';

abstract class MyCustomButton extends StatelessWidget {
  const MyCustomButton({
    this.child,
    this.roundRadius,
    this.buttonColor,
    this.buttonOverlayColor,
    this.buttonShadowColor,
    this.onPressed,
    this.padding,
  });

  final Widget? child;
  final double? roundRadius;
  final Color? buttonColor;
  final Color? buttonOverlayColor;
  final Color? buttonShadowColor;
  final EdgeInsets? padding;
  final void Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return ElevatedButton(
      onPressed: onPressed,
      child: Padding(
        padding:
            padding ?? (R.spaces.littleSpaceAround + R.spaces.littleHorizontal),
        child: child,
      ),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(roundRadius ?? 8),
          ),
        ),
        backgroundColor: MaterialStateProperty.all<Color>(
            buttonColor ?? theme.colorScheme.primary),
        // overlayColor: MaterialStateProperty.all<Color>(
        //     buttonOverlayColor ?? theme.splashColor),
        elevation: MaterialStateProperty.all<double>(0),
        padding: MaterialStateProperty.all<EdgeInsets>(
            padding ?? R.spaces.tinySpaceAround),
      ),
    );
  }
}
