import 'package:flutter/material.dart';

import '../../resources/r.dart';
import 'my_custom_button.dart';

class RoundedElevatedButton extends MyCustomButton {
  const RoundedElevatedButton({
    required Widget? child,
    required void Function()? onPressed,
    double? roundRadius,
    Color? buttonColor,
    Color? buttonOverlayColor,
    Color? buttonShadowColor,
    EdgeInsets? padding,
  }) : super(
          child: child,
          roundRadius: roundRadius,
          buttonColor: buttonColor,
          buttonOverlayColor: buttonOverlayColor,
          buttonShadowColor: buttonShadowColor,
          onPressed: onPressed,
          padding: padding,
        );

  factory RoundedElevatedButton.icon({
    required void Function() onPressed,
    required Widget icon,
    required Widget child,
    double? roundRadius,
    Color? buttonColor,
    Color? buttonOverlayColor,
    Color? buttonShadowColor,
    EdgeInsets? padding,
  }) = _RoundedButtonWithIcon;
}

class _RoundedButtonWithIcon extends RoundedElevatedButton {
  _RoundedButtonWithIcon({
    double? roundRadius,
    Color? buttonColor,
    Color? buttonOverlayColor,
    Color? buttonShadowColor,
    EdgeInsets? padding,
    void Function()? onPressed,
    Widget? icon,
    Widget? child,
  }) : super(
          child: _RoundedButtonWithIconChild(
            icon: icon,
            child: child,
          ),
          roundRadius: roundRadius,
          buttonColor: buttonColor,
          buttonOverlayColor: buttonOverlayColor,
          buttonShadowColor: buttonShadowColor,
          onPressed: onPressed,
          padding: padding,
        );
}

class _RoundedButtonWithIconChild extends StatelessWidget {
  const _RoundedButtonWithIconChild({
    this.icon,
    this.child,
  });
  final Widget? icon;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        child!,
        R.spaces.littleSpaceWidget,
        icon!,
      ],
    );
  }
}
