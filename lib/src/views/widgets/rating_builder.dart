import 'package:app/src/views/resources/r.dart';
import 'package:flutter/material.dart';

class RatingBuilder extends StatelessWidget {
  const RatingBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Row(
      children: <Widget>[
        ...List<Widget>.generate(
          5,
          (int index) => Icon(
            Icons.star,
            color: Colors.yellow,
          ),
        ),
        R.spaces.tinySpaceWidget,
        Text(
          '5.0',
          style: R.styles.normalFont.copyWith(
            color: theme.colorScheme.onBackground,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
