import 'package:app/src/views/resources/r.dart';
import 'package:flutter/material.dart';

class SeparatedText extends StatelessWidget {
  const SeparatedText({
    Key? key,
    required this.label,
    required this.value,
    this.labelColor,
    this.valueColor,
    this.padding,
    this.isValueBold = false,
    this.onValuePressed,
  }) : super(key: key);

  final String label;
  final Color? labelColor;
  final String value;
  final Color? valueColor;
  final EdgeInsets? padding;
  final bool isValueBold;
  final VoidCallback? onValuePressed;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);

    return Padding(
      padding: padding ?? EdgeInsets.zero,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            label,
            style: R.styles.normalFont.copyWith(
              color: labelColor ?? theme.colorScheme.secondaryVariant,
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: GestureDetector(
                onTap: onValuePressed,
                child: Text(
                  value,
                  style: R.styles.normalFont.copyWith(
                    color: valueColor ?? theme.colorScheme.onSurface,
                    fontWeight:
                        isValueBold ? FontWeight.bold : FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
