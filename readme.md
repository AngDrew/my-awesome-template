# MyAwesomeTemplate
**An awesome flutter template.** With this template you can simplify your work a lot (just my opinion 😉). This template is built with MVVM architecture and heavily depend on Provider, GetIt, Dio. I started create this template because I have to reuse my own code while doing freelance. Because I'm a type of lazy guy, I decided to make a shortcut. So I created this template just for me. But, it's always open source and I'll leave them on my gitlab open. Any contribution are welcome, and you don't need my permission to reuse the code 👍. I will be happy to help the whole community and bringing possitive impact to everyone.

## Guideline
This guideline is the best practice to use the template. You can rely on this guideline. But if you have a better opinion, I would like to have a chat with you~ Just create issues on the gitlab and we can discuss about that! I'd love to learn from anyone no matter what, and I'll try to improve the project as good as possible.

1. [Consuming the services] The best way to consume **Services** are from **View Model**; The **View Model** is the port of the UI and the Logic components. Every action that requires logic will be handled in the **View Model**.
2. [About view] Anything related to **View** stuff shall be handled in the view class itself. (Animation, Navigator, UI based on **current state**)
3. [States] the states are divided by 2 states. don't get confused and stay with me 😄. They're slightly different, but the concepts are the same!
	> Our state: this state is created manually by us, you can see them on `BaseViewModel` class. this state is used for indicator of the app. whether the app is busy or idle. when the state is busy, the `View Model` will request the `View` to rebuild specific widget to indicate the busy state. Thanks to provider's package, it has `Consumer` to listen to specific `ViewModel` class.
	code example: 
	```dart
	Consumer<LoginViewModel>(
		builder: (
			BuildContext context, 
			LoginViewModel value, 
			Widget? child,
		) => 
			RoundedElevatedButton(
				onPressed: value.isStateBusy ? 
					null : value.onPressed, 
				child: value.isStateBusy ? 
					CustomIndicator() : Text('Press me'),
			),
	);
	```
	>in the example above, we can swap the UI state from idle to busy and vice versa. The UI of the button will be re-built, and when the state is busy, the button will show circular progress indicator to indicate the app is processing user's request.
	>
	>the other state is:
	>the **'state' from Flutter** itself: this is the state back when we learn the difference between stateful widget and stateless widget. There are a widget that can change the looks by simply using `setState()` whenever there's a data changed, and the UI will be re rebuilt. Then the new data value will be shown like when we create a new flutter project, the default counter mechanic.
	>
	> They have the same concept. Just imagine the `setState()` render the whole widget tree while the `ScopedModel/Provider/Consumer` only render the specific part of the widget tree. You can learn more about state management of Flutter from [here](https://flutter.dev/docs/development/data-and-backend/state-mgmt/options). This template is using **provider** as the state management and **get_it** to help achieve certain accessibility of the code and of course to increase readability, easier work.
4. [Repositories] If you want to add a API integration to your app, you can make a repository class to interact with certain API endpoint. and you can make a API endpoint list as a constants file, so you don't have to check the whole repositories to change the url. You can use your own version here, not a problem.

## Lib
These dependencies will also makes your work even more easy. But add with caution because too many library will makese your project slow and too dependant.
Recommended library dependency:
 - flutter_screenutil
 - flutter_secure_storage
 - google_fonts
 - intl
 - shared_preferences
 - moor
 - freezed
 - flare_flutter
 - ?

## Installing
1. TLDR; Just make sure the template can fits your project. Don't overwrite what you don't want to overwrite, copy what you think you need. But there's a core of the template, if you don't copy them maybe you have to change a bit the code.
2. ->_<- 
3. Check the pubspec.yaml dart version of this project and yours
4. You must adjust the template accordingly
5. Don't copy pubspec.yaml instead, just copy some part of it like the dependencies, etc. if you copy your pubspec.yaml, you might break your project accidentally.
6. Copy the template to your project except pubspec.yaml and other thing you don't want to overwrite. You have to read them
7. Don't forget to re-check the pubspec.yaml
8. Run `flutter pub get` in terminal to get the dependencies from the pub server
9. Everything is set!

## Features
1. You can see at `.vscode/launch.json` there's an config example to run octopus mode. You just have to select the "All Device" in the flutter debugger vscode. you have to change the config by using your own devices id. (you can see those at `flutter devices`)
2. `l10n` or `localizations`. If you want to disable the Internalization config, you have to delete the `l10n.yaml` in the project root file and also you will have to delete the `lib/l10n` folder because that won't be necessary anymore. but if you want to enable them, you can check the step by step at [flutter website](https://flutter.dev/docs/development/accessibility-and-localization/internationalization)
	> Internalizations is adding a multiple language support to the app we're trying to make. You have to set up the localizations for every language and you have to decide what language to use, etc. you can read the detailed information, you can check on **flutter website** link above

3. `GlobalKeyService` with the help of get_it library, now it's possible to use context without actual context. Literally, you can access the current state context by using this service. you can grab the `GlobalKeyService` by using locator. NOTE: you can't get some context object using this context. But AFAIK, you can grab provider from context, showing Dialog, showing SnackBar, **pop**ping Dialod & Snackbar. For the best pratice, you have to consume this service from ViewModel.
	```dart
	  GlobalKeyService _key = locator<GlobalKeyService>();
	  BuildContext? context = _key.navigatorKey.currentState?.context;
	```
4. App Flavor! Yes you can add flavor support to the app. this action is built manually, there's no such flavor stuff in flutter. But if you interested, you can make your own:
```Dart
enum Environment { dev, prod }

// ignore: avoid_classes_with_only_static_members

class FlavorControl {
	static late Map<String, dynamic> _config;
	
	static Map<String, dynamic> get config =>
		Map<String, dynamic>.unmodifiable(_config);
		
	static void setEnvironment(Environment env) {
		switch (env) {
			case Environment.dev:
				_config = <String, dynamic>{
					'server': 'https://dev-admin.legalspace.id/api',
					'app_prefix': ' Dev Build',
				};
				break;
			case Environment.prod:
				_config = <String, dynamic>{
					'server': 'https://dev-admin.legalspace.id/api',
					'app_prefix': '',
				};
				break;
		}
	}
}
```
now, you can access `config` and use it accodringly~ for example if you want to use the server base url, you can go to `NetworkService`, then change the baseUrl to `config['server']` done you have separated server for every build!
NOTE: don't forget to call `FlavorControl.serEnvironment(Environment.dev)` in the `void main()` to apply the Flavor!
